import {StyleSheet, Text, View, Button, TextInput} from 'react-native';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Storage() {
  const [apakahLoading, setapkahLoading] = useState(true);
  const [penghitung, setPenghitung] = useState(0);
  const [salam, setSalam] = useState('');
  const [nama, setNama] = useState('');
  const [infoSalam, setInfoSalam] = useState();

  const getData = async () => {
    const values = await AsyncStorage.multiGet(['@hitung', '@salam']);

    values.forEach(value => {
      if (value[0] === '@hitung') {
        const hitung = parseInt(value[1]);
      } else if (value[0] === '@salam') {
        setInfoSalam(JSON.parse(value[1]));
      }
    });
    setapkahLoading(false);
  };

  useEffect(getData);

  if (apakahLoading) {
    return (
      <View>
        <Text>Loading....</Text>
      </View>
    );
  }

  const incrementCounter = async () => {
    await AsyncStorage.setItem('@hitung', (penghitung + 1).toString());
    setPenghitung(penghitung + 1);
  };

  const saveGreeting = async () => {
    const greetingToSave = {
      salam: salam,
      nama: nama,
    };
    await AsyncStorage.setItem('@salam', JSON.stringify(greetingToSave));
    setInfoSalam(greetingToSave);
  };

  return (
    <View>
      <Text>Count: {penghitung}</Text>
      <Button title="Increment" onPress={incrementCounter} />
      <View></View>
      <Text>Saved Greeting:</Text>
      {infoSalam ? (
        <Text>
          {infoSalam.salam} {infoSalam.nama}
        </Text>
      ) : (
        <Text>None :</Text>
      )}
      <TextInput onChangeText={setSalam} placeholder="salam" value={salam} />
      <TextInput onChange={setNama} placeholder="nama" value={nama} />
      <Button title="Save" onPress={saveGreeting} />
    </View>
  );
}

const styles = StyleSheet.create({});
