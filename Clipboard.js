import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import Clipboard from '@react-native-community/clipboard';
import {useState} from 'react';

export default function Klipbor() {
  const [Input, setInput] = useState('');

  const nomorHp = '085866294698';
  const copyText = text => {
    Clipboard.setString(text);
  };

  return (
    <View>
      <Text style={styles.nomorhp}>{nomorHp}</Text>
      <TouchableOpacity
        onPress={() => copyText(nomorHp)}
        style={styles.tombolcopy}>
        <Text style={styles.tomboltext}>Salin Nomor HP ke Clipboard</Text>
      </TouchableOpacity>
      <TextInput
        onChangeText={setInput}
        value={Input}
        style={styles.inputbox}></TextInput>
    </View>
  );
}

const styles = StyleSheet.create({
  nomorhp: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
  },
  tombolcopy: {
    margin: 10,
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#000',
    justifyContent: 'center',
  },
  tomboltext: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  inputbox: {
    margin: 10,
    padding: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000',
  },
});
