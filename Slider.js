import {StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import Slider from '@react-native-community/slider';

export default function Geser() {
  const [jarak, setJarak] = useState('50%');
  const [gerak, setGerak] = useState('Diam');

  return (
    <View style={styles.wadah}>
      <Text style={styles.text}>{jarak}</Text>
      <Text style={styles.text}>{gerak}</Text>
      <Slider
        minimumValue={0}
        maximumValue={1}
        minimumTrackTintColor="blue"
        maximumTrackTintColor="grey"
        value={0.5}
        thumbTintColor="blue"
        onValueChange={value => setJarak(parseInt(value * 100) + '%')}
        onSlidingStart={() => setGerak('Bergeser')}
        onSlidingComplete={() => setGerak('Diam')}></Slider>
    </View>
  );
}

const styles = StyleSheet.create({
  wadah: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    color: 'black',
  },
});
