import {Text, View, Button, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Netinfo from './Netinfo';
import Datetimepicker from './Datetimepicker';
import Slider from './Slider';
import Geolocation from './Geolocation';
import Progressview from './Progressview';
import Progressbarandroid from './Progressbarandroid';
import Clipboard from './Clipboard';
import Asyncstorage from './Asyncstorage';
import Snapcarousel from './Snapcarousel';
import Imagezoomviewer from './Imagezoomviewer';
import Lineargradient from './Lineargradient';
import Renderhtml from './Renderhtml';
import Share from './Share';
import Skeletonplaceholder from './Skeletonplaceholder';
import Webview from './Webview';
import Rntooltip from './Rntooltip';
import Vectoricon from './Vectoricon';

const Tombol = ({tujuan, namatombol, navigation}) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: 'white',
        width: '70%',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 5,
      }}
      onPress={() => navigation.navigate(tujuan)}>
      <Text
        style={{
          fontSize: 15,
          fontWeight: 'bold',
          color: 'black',
          padding: 5,
        }}>
        {namatombol}
      </Text>
    </TouchableOpacity>
  );
};

function Home({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'grey',
      }}>
      <Text
        style={{
          fontSize: 20,
          fontWeight: 'bold',
          color: 'white',
          padding: 5,
          textAlign: 'center',
        }}>
        React Native Library
      </Text>
      <Tombol tujuan="Netinfo" namatombol="Net Info" navigation={navigation} />
      <Tombol
        tujuan="Datetimepicker"
        namatombol="Date Time Picker"
        navigation={navigation}
      />
      <Tombol tujuan="Slider" namatombol="Slider" navigation={navigation} />
      <Tombol
        tujuan="Geolocation"
        namatombol="Geolocation"
        navigation={navigation}
      />
      <Tombol
        tujuan="Progressview"
        namatombol="Progress View"
        navigation={navigation}
      />
      <Tombol
        tujuan="Progressbarandroid"
        namatombol="Progress Bar Android"
        navigation={navigation}
      />
      <Tombol
        tujuan="Clipboard"
        namatombol="Clipboard"
        navigation={navigation}
      />
      <Tombol
        tujuan="Asyncstorage"
        namatombol="Async Storage"
        navigation={navigation}
      />
      <Tombol
        tujuan="Snapcarousel"
        namatombol="Snap Carousel"
        navigation={navigation}
      />
      <Tombol
        tujuan="Imagezoomviewer"
        namatombol="Image Zoom Viewer"
        navigation={navigation}
      />
      <Tombol
        tujuan="Lineargradient"
        namatombol="Linear Gradient"
        navigation={navigation}
      />
      <Tombol
        tujuan="Renderhtml"
        namatombol="Render HTML"
        navigation={navigation}
      />
      <Tombol tujuan="Share" namatombol="Share" navigation={navigation} />
      <Tombol
        tujuan="Skeletonplaceholder"
        namatombol="Skeleton Placeholder"
        navigation={navigation}
      />
      <Tombol tujuan="Webview" namatombol="Web View" navigation={navigation} />
      <Tombol
        tujuan="Rntooltip"
        namatombol="RN Tooltip"
        navigation={navigation}
      />
      <Tombol
        tujuan="Vectoricon"
        namatombol="Vector Icon"
        navigation={navigation}
      />
    </View>
  );
}

const Stack = createNativeStackNavigator();

function MyStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Netinfo" component={Netinfo} />
        <Stack.Screen name="Datetimepicker" component={Datetimepicker} />
        <Stack.Screen name="Slider" component={Slider} />
        <Stack.Screen name="Geolocation" component={Geolocation} />
        <Stack.Screen name="Progressview" component={Progressview} />
        <Stack.Screen
          name="Progressbarandroid"
          component={Progressbarandroid}
        />
        <Stack.Screen name="Clipboard" component={Clipboard} />
        <Stack.Screen name="Asyncstorage" component={Asyncstorage} />
        <Stack.Screen name="Snapcarousel" component={Snapcarousel} />
        <Stack.Screen name="Imagezoomviewer" component={Imagezoomviewer} />
        <Stack.Screen name="Lineargradient" component={Lineargradient} />
        <Stack.Screen name="Renderhtml" component={Renderhtml} />
        <Stack.Screen name="Share" component={Share} />
        <Stack.Screen
          name="Skeletonplaceholder"
          component={Skeletonplaceholder}
        />
        <Stack.Screen name="Webview" component={Webview} />
        <Stack.Screen name="Rntooltip" component={Rntooltip} />
        <Stack.Screen name="Vectoricon" component={Vectoricon} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default MyStack;
