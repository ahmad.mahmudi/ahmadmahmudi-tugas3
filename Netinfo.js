import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import NetInfo from '@react-native-community/netinfo';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

function Netinfo({navigation}) {
  const netInfo = NetInfo.useNetInfo();

  return (
    <View style={styles.container}>
      <Text style={styles.text1}>Type : </Text>
      <Text style={styles.text2}> {netInfo.type}</Text>
      <Text style={styles.text1}>Is Expesinve : </Text>
      <Text style={styles.text2}>
        {netInfo.details?.isConnectionExpensive.toString()}
      </Text>
      <Text style={styles.text1}>Is Connected : </Text>
      <Text style={styles.text2}> {netInfo.isConnected?.toString()}</Text>
      <Text style={styles.text1}>IP Address : </Text>
      <Text style={styles.text2}> {netInfo.details?.ipAddress}</Text>
      <Text style={styles.text1}>Frequency : </Text>
      <Text style={styles.text2}> {netInfo.details?.frequency}</Text>
      <Text style={styles.text1}>Strength : </Text>
      <Text style={styles.text2}> {netInfo.details?.strength}</Text>
      <Text style={styles.text1}>Carrier :</Text>
      <Text style={styles.text2}> {netInfo.details?.carrier}</Text>
      <TouchableOpacity
        style={styles.tombol}
        onPress={() => navigation.goBack()}>
        <Text style={styles.texttombol}>Go Back</Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createNativeStackNavigator();

export default function MyStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="NetinfoPage" component={Netinfo}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
    display: 'inline-block',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text1: {
    width: '40%',
    textAlign: 'center',
  },
  text2: {
    fontWeight: 'bold',
    width: '40%',
    textAlign: 'center',
  },
  tombol: {
    backgroundColor: 'grey',
    width: '40%',
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    position: 'absolute',
    bottom: 10,
  },
  texttombol: {
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
  },
});
