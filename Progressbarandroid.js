import {StyleSheet, Text, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {ProgressBar} from '@react-native-community/progress-bar-android';

const progressProps = {
  styleAttr: 'Horizontal',
  indeterminate: false,
};

export default function Progressbarandroid() {
  const [progress, setProgress] = useState(0);
  useEffect(() => {
    function updateProgress() {
      setProgress(currenProgress => {
        if (currenProgress < 1) {
          setTimeout(updateProgress, 500);
        }
        return currenProgress + 0.01;
      });
    }
    updateProgress();
  }, []);
  return (
    <View>
      <Text>{Math.round(progress * 100)}%</Text>
      <ProgressBar {...progressProps} progress={progress}></ProgressBar>
    </View>
  );
}

const styles = StyleSheet.create({});
