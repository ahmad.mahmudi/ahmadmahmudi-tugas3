import {View, Text} from 'react-native';
import React from 'react';
import {ProgressView} from '@react-native-community/progress-view';

const Progressview = () => {
  return (
    <View>
      <Text>progressview</Text>
      <ProgressView
        progressTintColor="red"
        progress={0.7}
        trackTintColor="black"></ProgressView>
    </View>
  );
};

export default Progressview;
