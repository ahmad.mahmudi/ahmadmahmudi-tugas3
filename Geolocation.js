import {
  StyleSheet,
  Text,
  View,
  PermissionsAndroid,
  TouchableOpacity,
  Linking,
} from 'react-native';
import React from 'react';
import Geolocation from '@react-native-community/geolocation';
import {useState} from 'react';

export default function Lokasi() {
  const Permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Tugas3 App Camera Permission',
          message: 'Tugas3 App needs access to your location ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use location permission');
        getLokasiSekarang();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const [lokasiSekarang, setLokasiSekarang] = useState(null);
  const getLokasiSekarang = () => {
    Geolocation.getCurrentPosition(
      position => {
        setLokasiSekarang({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      },
      error => console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 1000,
      },
    );
  };
  const bukaPeta = () => {
    const {latitude, longitude} = lokasiSekarang;
    if ((latitude, longitude)) {
      const url = `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
      Linking.openURL(url);
    } else {
      alert('location not available');
    }
  };

  return (
    <View style={styles.wadah}>
      <Text>Koordinat</Text>
      <View>
        <Text>
          Latitude: {lokasiSekarang ? lokasiSekarang.latitude : 'Loading'}
        </Text>
        <Text>
          Longitude: {lokasiSekarang ? lokasiSekarang.longitude : 'Loading'}
        </Text>
      </View>
      {lokasiSekarang ? (
        <>
          <TouchableOpacity onPress={bukaPeta}>
            <Text>Buka Maps</Text>
          </TouchableOpacity>
        </>
      ) : (
        <TouchableOpacity onPress={Permission}>
          <Text>Dapatkan Lokasi</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  wadah: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
