import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

function Datetimepicker({navigation}) {
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [mode, setMode] = useState('date');

  const onChange = (e, selectedDate) => {
    setDate(selectedDate);
    setShow(false);
  };

  const showMode = modeToShow => {
    setShow(true);
    setMode(modeToShow);
  };

  return (
    <View style={styles.wadah}>
      <TouchableOpacity style={styles.tombol} onPress={() => showMode('date')}>
        <Text style={styles.tomboltext}>Kalender</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tombol} onPress={() => showMode('time')}>
        <Text style={styles.tomboltext}>Jam</Text>
      </TouchableOpacity>
      {show && (
        <DateTimePicker
          value={date}
          mode={mode}
          is24Hour={true}
          onChange={onChange}
        />
      )}
      <Text style={styles.waktu}>{date.toLocaleDateString()}</Text>
      <Text style={styles.waktu}>{date.toLocaleTimeString()}</Text>
      <TouchableOpacity
        style={styles.tombolBack}
        onPress={() => navigation.goBack()}>
        <Text style={styles.texttombol}>Go Back</Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createNativeStackNavigator();

export default function MyStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="Date Time Picker"
        component={Datetimepicker}></Stack.Screen>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  wadah: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tombol: {borderRadius: 5, backgroundColor: 'grey', width: '50%', margin: 5},
  tomboltext: {textAlign: 'center', margin: 5, color: 'white'},
  waktu: {margin: 5, color: 'black', fontWeight: 'bold'},
  tombolBack: {
    backgroundColor: 'grey',
    width: '40%',
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 10,
    position: 'absolute',
    bottom: 10,
  },
  texttombol: {fontWeight: 'bold', textAlign: 'center', color: 'white'},
});
